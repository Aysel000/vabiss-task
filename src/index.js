import './style/styles.css';

import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/App';

import themes from 'devextreme/ui/themes';
themes.initialized(() => ReactDOM.render(
  <App />,
  document.getElementById('app')
));
