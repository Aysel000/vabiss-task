import React from "react";
import store from "../data/data"

import TreeList, {
  Column,
  RequiredRule,
  Editing,
  HeaderFilter,
  Button,
  SearchPanel,
  Lookup,
} from "devextreme-react/tree-list";

// import { employees } from "../data/data";

const expandedRowKeys = [1, 2, 3, 4, 5];

const headDataSource = {
  // store: employees,
  sort: "Full_Name",
};
console.log(store)

class App extends React.Component {
  render() {
    return (
      <div id="tree-list-demo">
        <TreeList
          id="employees"
         dataSource={store._array}
          showRowLines={true}
          showBorders={true}
          columnAutoWidth={true}
          defaultExpandedRowKeys={expandedRowKeys}
          keyExpr="ID"
          parentIdExpr="Head_ID"
          onEditorPreparing={onEditorPreparing}
          onInitNewRow={onInitNewRow}
        >
          <Editing
            allowUpdating={true}
            allowDeleting={true}
            allowAdding={true}
            mode="row"
          />
          <Column dataField="Full_Name">
            <RequiredRule />
          </Column>
         

          <SearchPanel visible={true} width={250} />

          <Column dataField="Status" caption="Status" minWidth={100}>
            <Lookup dataSource={statuses} />
          </Column>
          <Column type="buttons" dataField="Tools" caption="Tools">
            <Button name="edit" />
            <Button name="delete" />
          </Column>
        </TreeList>
      </div>
    );
  }
}
const onEditorPreparing = (e) => {
  if (e.dataField === "Head_ID" && e.row.data.ID === 1) {
    e.cancel = true;
  }
};
const onInitNewRow = (e) => {
  e.data.Head_ID = 1;
};



const statuses = ["Not Started", "Need Assistance", "Completed"];

export default App;
