import React from "react";

import LocalStore from "devextreme/data/local_store";
// import DataSource from "devextreme/data/data_source";

const employees = [
  {
    ID: 1,
    Head_ID: 0,
    Full_Name: "John Heart",
    Status: "Not Started",
 
  },
  {
    ID: 2,
    Head_ID: 1,
    Full_Name: "Samantha Bright",
    Status: "Not Started",
   
  },
  {
    ID: 3,
    Head_ID: 1,
    Full_Name: "Arthur Miller",
    Status: "completed",
   
  },
  {
    ID: 4,
    Head_ID: 1,
    Full_Name: "Robert Reagan",
    Status: "completed",
  
  },
  {
    ID: 5,
    Head_ID: 1,
    Full_Name: "Greta Sims",
    Status: "completed",
 
  },
  {
    ID: 6,
    Head_ID: 3,
    Full_Name: "Brett Wade",
    Status: "completed",
   
  },
  {
    ID: 7,
    Head_ID: 5,
    Full_Name: "Sandra Johnson",
    Status: "Need Assistance",

  },
  {
    ID: 8,
    Head_ID: 4,
    Full_Name: "Ed Holmes",
    Status: "Need Assistance",
   
  },
  {
    ID: 9,
    Head_ID: 3,
    Full_Name: "Barb Banks",
    Status: "completed",
  
  },
  {
    ID: 10,
    Head_ID: 2,
    Full_Name: "Kevin Carter",
    Status: "completed",
  
  },

];

const store = new LocalStore({
  key: "id",
  data: employees,
  name: "myLocalData",
  // Other LocalStore properties go here
});



export default store;
